---
eleventyNavigation:
  key: UsingGitIgnore
  title: Using .gitignore
  parent: Git
---

You should try to keep your Git repositories small and clean of any clutter.
Only the really necessary files (e.g. source files, documentation, etc.) should be committed.
This helps you in your daily work, and it helps Codeberg as we do not have to provide unnecessary disk space. 

Sometimes additional files are created within the same directory as your source files. These might for example be the
results of compilation of the source or some other work.

To prevent accidentally committing those files to your repository, you can add file and directory names to a file named `.gitignore`.

Files and directories that match the names in the `.gitignore` file are ignored by Git. They do not show up in `git status`
and they cannot be added to the staging area using `git add` (except when added with `--force`). 

Gitea (the forge software used by Codeberg) offers templates for `.gitignore` for common languages on creating a repository. 

## Further reading

- [Documentation on gitignore by the Git project](https://git-scm.com/docs/gitignore)
- [Section on gitignore in the book Pro Git 2nd edition](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_ignoring)