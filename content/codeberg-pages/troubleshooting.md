---
eleventyNavigation:
  key: Troubleshooting
  title: Troubleshooting
  parent: CodebergPages
  order: 99
---

## My web browser displays a security warning when I try to access my Codeberg Pages.
If your user name contains a dot (e.g. `user.name`) your Codeberg Pages URL (https://user.name.codeberg.page) contains a sub-sub-domain which does not work with Let's Encrypt wildcard certificates. Use the alternative URL https://pages.codeberg.org/user.name/ as a workaround.

## My content is not updated

The new Codeberg Pages Server uses caching for files under a certain size (currently 1 MiB).
Please wait a few minutes until cache is invalidated.
This is to improve performance, reduce server load and cost as well as save energy.
